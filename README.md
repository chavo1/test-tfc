# This repo contain a simple TF code to be used in Terraform Cloud that create EC2 in AWS

## Prerequisites
- AWS credential
- Add the credentials as an Environment Variables in your workspace

```
Key AWS_ACCESS_KEY_ID Value "<Your AWS_ACCESS_KEY_ID>"
Key AWS_SECRET_ACCESS_KEY Value "<Your AWS_SECRET_ACCESS_KEY>"
```
- Queue a plan

